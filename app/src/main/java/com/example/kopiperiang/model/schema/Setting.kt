package com.example.kopiperiang.model.schema

open class Setting (
    var statusOpen : String? = "",
    var openTime : String? = "",
    var closingTime : String? = ""
)

