package com.example.kopiperiang.model.schema

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Menu (
    var menuOrderID : String? = "",
    var menuID : String? = "",
    var categoryID : String? = "",
    var menuName : String? = "",
    var description : String? = "",
    var qty : String? = "",
    var price : String? = "",
    var image : String? = "",
    var sugar : String? = "",
    var ice : String? = "",
    var totalPrice : Int = 0
) : Parcelable

