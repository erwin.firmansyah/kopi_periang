package com.example.kopiperiang.model.schema

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Transaction (
    var transactionID : String? = "",
    var transactionDate : String? = "",
    var orderDate : String? = "",
    var tableNo : String? = "",
    var orderList : ArrayList<Menu>? = arrayListOf(),
    var grandTotal : String?= "",
    var discount : Int?= 0,
    var charge : Int?= 0,
    var customerName : String?= "",
    var orderStatus : Int?= 0
) : Parcelable

