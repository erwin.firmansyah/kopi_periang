package com.example.kopiperiang.base

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewbinding.ViewBinding
import com.example.kopiperiang.R
import com.example.kopiperiang.feature.admin.AdminPageActivity
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.utils.PreferenceHelper
import com.example.kopiperiang.feature.order.SetupOrderActivity
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.FirebaseFirestore

abstract class BaseFragment<T : BaseViewModel, V : ViewBinding> : Fragment() {

//    lateinit var mViewModelFactory: ViewModelProvider.Factory

    lateinit var mViewModel: T
    lateinit var pref : PreferenceHelper
    val mViewDataBinding get() = _mViewDataBinding!!
    val handler = Handler()
    var firebaseDB = FirebaseFirestore.getInstance()

    private var _mViewDataBinding: V? = null

    abstract fun setViewModel()

    /**
     *  Initialisasi Data Binding
     *  DataBindingUtil.inflate(inflater, R.layout.fragment_my_program, container, false)
     *
     *  Initialisasi View Binding
     *  FragmentMyProgramBinding.inflate(inflater, container, false)
     */
    abstract fun getViewBinding(inflater: LayoutInflater, container: ViewGroup?): V
    abstract fun getMainContainer(): View?


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _mViewDataBinding = getViewBinding(inflater, container)
        return mViewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref = PreferenceHelper(requireContext())
    }

    override fun onDestroyView() {
        _mViewDataBinding = null
        super.onDestroyView()
    }

    fun startNavigationTo(activityTarget: Class<*>, bundle: Bundle) {
        val intent = Intent(activity, activityTarget)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun startNavigationTo(context: Context, activityTarget: Class<*>, bundle: Bundle) {
        val intent = Intent(context, activityTarget)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun startNavigationNewTaskTo(activityTarget: Class<*>, bundle: Bundle) {
        val intent = Intent(activity, activityTarget)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun startNavigationResultTo(activityTarget: Class<*>, bundle: Bundle, resultCode: Int) {
        val intent = Intent(activity, activityTarget)
        intent.putExtras(bundle)
        startActivityForResult(intent, resultCode)
    }

    fun finishActivityWithResult(activityFragment: Activity, bundle: Bundle, resultCode: Int) {
        val intent = Intent()
        intent.putExtras(bundle)
        activityFragment.setResult(resultCode, intent)
        activityFragment.finish()
    }

    fun goToFragment(fm : FragmentManager, fragment : Fragment, bundle : Bundle?=null, tag : String?){
        val fragmentTransaction : FragmentTransaction = fm.beginTransaction()
        if(bundle != null){
            fragment.arguments = bundle
        }
        if(fragment.isAdded)
            return
        fragmentTransaction.replace(R.id.container_fragment, fragment, tag)
        fragmentTransaction.commit()
//        if(fm.findFragmentByTag(tag) == null && tag != null)
        fragmentTransaction.addToBackStack(tag)
    }

    fun shareNews(url : String) {
        val intent = Intent()
        intent.setAction(Intent.ACTION_SEND)
        intent.setType("text/plain")
        intent.putExtra(Intent.EXTRA_TEXT, url)
        startActivity(Intent.createChooser(intent, "Share"))
    }

    fun goToSetupOrderPage(fragmentIndex : Int, bundle : Bundle) {
        bundle.putInt(GlobalConstant.Extras.FRAGMENT_INDEX, fragmentIndex)
        val intent = Intent(requireContext(), SetupOrderActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun goToOrderPage(fragmentIndex : Int, bundle : Bundle) {
        bundle.putInt(GlobalConstant.Extras.FRAGMENT_INDEX, fragmentIndex)
        val intent = Intent(requireContext(), OrderActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun goToAdminPage(fragmentIndex : Int, bundle : Bundle) {
        bundle.putInt(GlobalConstant.Extras.FRAGMENT_INDEX, fragmentIndex)
        val intent = Intent(requireContext(), AdminPageActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    fun newInstanceFragment(bundle: Bundle, fragment: Fragment): Fragment {
        fragment.arguments = bundle
        return fragment
    }
}