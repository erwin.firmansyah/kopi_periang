package com.example.kopiperiang.base

import androidx.lifecycle.ViewModel
import com.example.kopiperiang.rest.api.ApiService
import com.example.kopiperiang.rest.module.RetrofitModule
import com.example.kopiperiang.utils.PreferenceHelper

abstract class BaseViewModel : ViewModel() {

}