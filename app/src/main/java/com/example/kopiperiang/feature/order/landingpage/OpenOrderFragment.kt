package com.example.kopiperiang.feature.order.landingpage

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentOpeningOrderBinding
import com.example.kopiperiang.feature.order.SetupOrderActivity
import com.example.kopiperiang.feature.order.insertname.InsertNameFragment
import com.example.kopiperiang.model.schema.Setting
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mu.sekolah.android.extension.defaultEmpty

class OpenOrderFragment : BaseFragment<EmptyViewModel, FragmentOpeningOrderBinding>() {

    private var setting = Setting()
    private var statusCafe = ""
    private var operationalWorkCafe = ""

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOpeningOrderBinding =
        FragmentOpeningOrderBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        pref.setTransaction("")
        pref.setTransactionListOnState("")
        getDataSetting()
        setupView()
    }

    override fun onResume() {
        (activity as SetupOrderActivity).state = 0
        super.onResume()
    }

    private fun setupView(){
        mViewDataBinding.apply {
            statusCafe = if(setting.statusOpen == "Close") "Oh no, We’re Closed" else "Yes, We're Open"
            operationalWorkCafe = setting.openTime + " - " + setting.closingTime

            tvOpen.text = statusCafe
            tvOperational.text = operationalWorkCafe
            btnInstagram.setOnClickListener {
                val uri = Uri.parse("https://www.instagram.com/kopi.periang/")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                intent.setPackage("com.instagram.android");
                try {
                    startActivity(intent)
                }catch (e : ActivityNotFoundException){
                    Toast.makeText(requireContext(), "Instagram Not Installed", Toast.LENGTH_SHORT).show()
                }
            }
            btnGetStarted.setOnClickListener {
                goToFragment(parentFragmentManager, InsertNameFragment(), Bundle(), "")
            }
        }
    }

    private fun getDataSetting(){
        Log.d("DATA", "DATA : " + pref.getSettingCafe())
        setting = Gson().fromJson(pref.getSettingCafe(), Setting::class.java)
    }
}