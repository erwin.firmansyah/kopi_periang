package com.example.kopiperiang.feature.admin.menulist

import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.databinding.LayoutMenuDetailAdminBinding
import com.example.kopiperiang.listener.OnItemClickListenerAdmin
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import javax.inject.Inject

class MenuListAdminViewHolder@Inject constructor(private val dataBinding: LayoutMenuDetailAdminBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Menu, onItemClickListener: OnItemClickListenerAdmin) {

        dataBinding.apply {
            tvMenuName.text = data.menuName
            val itemPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.price.defaultEmpty(), false).defaultEmpty()
            tvPrice.text = itemPrice
            tvDescription.text = data.description
            GlideHelper.loadImageUrl(root.context, data.image.defaultEmpty(), ivMenu)

            ivDelete.setOnClickListener {
                onItemClickListener.onDeleteClick(data)
            }
        }
    }
}