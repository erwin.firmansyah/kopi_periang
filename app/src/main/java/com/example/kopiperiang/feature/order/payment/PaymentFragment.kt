package com.example.kopiperiang.feature.order.payment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentOrderPaymentBinding
import com.example.kopiperiang.extension.setupLinear
import com.example.kopiperiang.feature.admin.AdminPageActivity
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.feature.order.home.MenuFragment
import com.example.kopiperiang.feature.order.list.OrderListFragment
import com.example.kopiperiang.listener.OnQtyChangeListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.example.kopiperiang.utils.StringHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mu.sekolah.android.extension.defaultEmpty

class PaymentFragment: BaseFragment<EmptyViewModel, FragmentOrderPaymentBinding>(),
    OnQtyChangeListener {
    private var name = ""
    private var transaction = Transaction()
    private var totalPrice = 0
    private var tax = 0
    private var grandTotal = 0
    private lateinit var detailOrderAdapter : PaymentAdapter

    override fun setViewModel() {
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_TRANSACTION)) {
            transaction = requireArguments().getParcelable(GlobalConstant.Extras.EXTRAS_TRANSACTION) ?: Transaction()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOrderPaymentBinding =
    FragmentOrderPaymentBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView() {
        (activity as OrderActivity).transactionLiveCycle = true
        pref.setTransaction(Gson().toJson(transaction))
        name = pref.getCustomerName().defaultEmpty()
        detailOrderAdapter = PaymentAdapter()
        mViewDataBinding.rvMenu.setupLinear(requireContext(), detailOrderAdapter)
        detailOrderAdapter.updateData(transaction.orderList ?: arrayListOf())
        detailOrderAdapter.setListener(this)

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.table,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            mViewDataBinding.dropdownTable.adapter = adapter
        }

        mViewDataBinding.btnOrder.setOnClickListener {
            saveTransaction()
        }

        mViewDataBinding.btnHomeOrder.setOnClickListener {
            (activity as OrderActivity).onBackPressed()
        }

        changeStateButtonOrder()
        setupTotalPayment()
    }

    override fun onQtyChange(menuOrderID : String, qty : Int) {
        if(qty==0){
            (activity as OrderActivity).customDialog.setupDialog("Apakah anda yakin ingin menghapus menu ini ?", true,
                { changeData(menuOrderID, qty) }, R.color.black)
        }else{
            changeData(menuOrderID, qty)
        }
    }

    private fun changeData(menuOrderID : String, qty : Int){
        var menu = Menu()
        transaction.orderList?.forEach {
            if(it.menuOrderID == menuOrderID){
                it.qty = qty.toString()
                menu = it
            }
        }

        if(qty==0){
            transaction.orderList?.remove(menu)
            detailOrderAdapter.updateData(transaction.orderList ?: arrayListOf())
            (activity as OrderActivity).customDialog.dismissDialog()
        }

        pref.setTransaction(Gson().toJson(transaction))

        changeStateButtonOrder()
        setupTotalPayment()
    }

    private fun saveTransaction(){
        val jsonTransaction = pref.getTransactionListOnState()
        val listTransaction: ArrayList<String> = Gson().fromJson(jsonTransaction, object : TypeToken<List<String?>?>() {}.type) ?: arrayListOf()
        transaction.transactionDate = StringHelper.getCurrentTime()
        transaction.orderDate = StringHelper.getDate()
        transaction.orderStatus = 1
        transaction.tableNo = mViewDataBinding.dropdownTable.selectedItem.toString()
        firebaseDB.collection("transaction")
            .add(transaction)
            .addOnSuccessListener {
                (activity as OrderActivity).successDialog.startLoading()
                (activity as OrderActivity).transactionLiveCycle = false
                handler.postDelayed({
                    (activity as OrderActivity).successDialog.isDismiss()
                    (activity as OrderActivity).customDialog.setupDialog("Pesanan Berhasil", false, ::nullFun, R.color.green)
                    listTransaction.add(transaction.transactionID.defaultEmpty())
                    pref.setTransactionListOnState(Gson().toJson(listTransaction))
                    pref.setTransaction("")
                    goToOrderList()
                },2000)
            }
            .addOnFailureListener {
                (activity as OrderActivity).failedDialog.startLoading()
                handler.postDelayed({
                    (activity as OrderActivity).failedDialog.isDismiss()
                    (activity as OrderActivity).customDialog.setupDialog("Gagal Memesan, Silahkan Coba Lagi", false, ::nullFun, R.color.red)
                    dismissCustomDialog()
                },3000)
            }
    }

    private fun goToOrderList(){
        handler.postDelayed({
            (activity as OrderActivity).customDialog.dismissDialog()
            goOrderList()
        },3000)
    }

    private fun dismissCustomDialog(){
        handler.postDelayed({
            (activity as AdminPageActivity).customDialog.dismissDialog()
        },2000)
    }

    private fun setupTotalPayment(){
        totalPrice = 0
        tax = 0
        grandTotal = 0

        transaction.orderList?.forEach {
            totalPrice += it.totalPrice
        }
        tax = totalPrice/10
        grandTotal = totalPrice + tax

        mViewDataBinding.apply {
            detailPayment.tvPrice.text = StringHelper.convertToCurrencyFormat(totalPrice.toString(), false)
            detailPayment.tvTax.text = StringHelper.convertToCurrencyFormat(tax.toString(), false)
            detailPayment.tvTotal.text = StringHelper.convertToCurrencyFormat(grandTotal.toString(), false)
        }

        transaction.grandTotal = grandTotal.toString()
    }

    private fun changeStateButtonOrder(){
        if(transaction.orderList?.size==0){
            mViewDataBinding.btnOrder.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_disabled_oval_btn)
            mViewDataBinding.btnOrder.isEnabled = false
        }else{
            mViewDataBinding.btnOrder.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_green_oval_btn)
            mViewDataBinding.btnOrder.isEnabled = true
        }
    }

    private fun newInstanceOrderList(bundle: Bundle): OrderListFragment {
        val fragment = OrderListFragment()
        fragment.arguments = bundle
        return fragment
    }

    private fun goOrderList() {
        val bundle = bundleOf(
            GlobalConstant.Extras.FROM_PAYMENT to true
        )
        val orderList = newInstanceOrderList(bundle)
        goToFragment(parentFragmentManager, orderList, bundle, "orderList")
        (activity as OrderActivity).state = 3
    }

    private fun nullFun(){}
}