package com.example.kopiperiang.feature.order.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuDetailBinding
import com.example.kopiperiang.listener.OnItemClickListener

class MenuDetailAdapter : RecyclerView.Adapter<MenuDetailViewHolder>() {

    private lateinit var data: List<Menu>
    private lateinit var onItemClickListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuDetailViewHolder {
        val viewBinding: LayoutMenuDetailBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_menu_detail, parent, false)
        return MenuDetailViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized) data.size else 0
    }
    override fun onBindViewHolder(holder: MenuDetailViewHolder, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            onItemClickListener.onClick(data[position])
        }
    }

    fun updateData(data: List<Menu>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun setListener(listener: OnItemClickListener){
        this.onItemClickListener = listener
    }
}