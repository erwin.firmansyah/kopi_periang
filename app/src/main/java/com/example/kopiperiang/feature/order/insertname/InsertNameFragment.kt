package com.example.kopiperiang.feature.order.insertname

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentInsertNameBinding
import com.example.kopiperiang.feature.order.SetupOrderActivity
import com.example.kopiperiang.utils.GlobalConstant
import java.util.*

class InsertNameFragment : BaseFragment<EmptyViewModel, FragmentInsertNameBinding>() {

    var newName = true

    override fun setViewModel() {

        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_NEW)) {
            newName = requireArguments().getBoolean(GlobalConstant.Extras.EXTRAS_NEW)
        }
    }
    override fun getViewBinding(

        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentInsertNameBinding =
        FragmentInsertNameBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupButton()
    }

    override fun onResume() {
        (activity as SetupOrderActivity).state = 1
        super.onResume()
    }

    private fun setupButton(){
        var name = ""
        if(!newName){
            mViewDataBinding.etName.hint = "Change Name"
        }else{
            mViewDataBinding.etName.hint = "Insert Name"
        }

        mViewDataBinding.btnSubmit.setOnClickListener {
            name = mViewDataBinding.etName.text.toString()

            if(name.isEmpty()){
                Toast.makeText(context, "Please insert name", Toast.LENGTH_SHORT).show()
            }else{
                if (name.toLowerCase(Locale.ROOT) == "admin"){
                    val dialogPassword = Dialog(requireActivity())
                    dialogPassword.setCancelable(true)
                    dialogPassword.setContentView(R.layout.layout_admin_login_dialog)
                    dialogPassword.show()

                    val etName = dialogPassword.findViewById(R.id.et_name) as EditText
                    val adminPassword = pref.getAdminPassword()
                    val btnSubmit = dialogPassword.findViewById(R.id.btn_submit) as Button

                    btnSubmit.setOnClickListener {
                        val password = etName.text.toString()
                        if(password == adminPassword){
                            goToAdminPage(GlobalConstant.FragmentIndex.ADMIN_PAGE, Bundle())
                            dialogPassword.dismiss()
                            mViewDataBinding.etName.setText("")
                        }else{
                            Toast.makeText(context, "Password doesn't match", Toast.LENGTH_SHORT).show()
                            dialogPassword.dismiss()
                        }
                    }

                }else{
                    pref.setCustomerName(name)
                    val bundle = bundleOf(
                        GlobalConstant.Extras.EXTRAS_NEW to newName
                    )
                    goToOrderPage(GlobalConstant.FragmentIndex.MENU, bundle)
                }
            }
        }
    }


}