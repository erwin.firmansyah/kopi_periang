package com.example.kopiperiang.feature.admin.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentHomeAdminBinding
import com.example.kopiperiang.databinding.FragmentOrderListBinding
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.model.schema.Setting
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mu.sekolah.android.extension.defaultEmpty
import android.widget.AdapterView

import android.widget.AdapterView.OnItemSelectedListener
import com.example.kopiperiang.feature.admin.list.OrderListAdminFragment
import com.example.kopiperiang.feature.admin.menulist.MenuListAdminFragment
import com.example.kopiperiang.feature.order.payment.PaymentFragment
import com.example.kopiperiang.listener.OnParentAdminActionListener
import com.example.kopiperiang.listener.OnSeeAllMenuListener


class HomeAdminFragment: BaseFragment<EmptyViewModel, FragmentHomeAdminBinding>(){

    private var setting = Setting()
    private var documentID = ""
    private var statusOpen = ""
    private var openTime = ""
    private var closeTime = ""
    private lateinit var onSeeMenu : OnParentAdminActionListener
    private lateinit var statusCafeAdapter : ArrayAdapter<CharSequence>
    private lateinit var openTimeAdapter : ArrayAdapter<CharSequence>
    private lateinit var closeTimeAdapter : ArrayAdapter<CharSequence>

    override fun setViewModel() {

    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeAdminBinding =
        FragmentHomeAdminBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    override fun onResume() {
        super.onResume()
        setupView()
    }

    private fun setupView() {
        mViewDataBinding.apply {
            statusCafeAdapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.status_cafe,
                R.layout.spinner_text_opening_status
            ).also { adapter ->
                adapter.setDropDownViewResource(R.layout.spinner_text_opening_status)
                dropdownStatus.adapter = adapter
            }

            openTimeAdapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.clock,
                R.layout.spinner_text_time
            ).also { adapter ->
                adapter.setDropDownViewResource(R.layout.spinner_text_opening_status)
                dropdownOpen.adapter = adapter
            }

            closeTimeAdapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.clock,
                R.layout.spinner_text_time
            ).also { adapter ->
                adapter.setDropDownViewResource(R.layout.spinner_text_opening_status)
                dropdownClose.adapter = adapter
            }

            getDataFromFirestore()

            dropdownStatus.onItemSelectedListener =
                object : OnItemSelectedListener {
                    override fun onItemSelected(
                        parentView: AdapterView<*>?,
                        selectedItemView: View,
                        position: Int,
                        id: Long
                    ) {
                        if (statusOpen != dropdownStatus.selectedItem.toString()){
                            statusOpen = dropdownStatus.selectedItem.toString()
                            updateSetting()
                        }
                    }

                    override fun onNothingSelected(parentView: AdapterView<*>?) {
                        // your code here
                    }
                }

            dropdownOpen.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(
                    parentView: AdapterView<*>?,
                    selectedItemView: View,
                    position: Int,
                    id: Long
                ) {
                    if (openTime != dropdownOpen.selectedItem.toString()){
                        openTime = dropdownOpen.selectedItem.toString()
                        updateSetting()
                    }
                }

                override fun onNothingSelected(parentView: AdapterView<*>?) {
                    // your code here
                }
            }

            dropdownClose.onItemSelectedListener =
                object : OnItemSelectedListener {
                    override fun onItemSelected(
                        parentView: AdapterView<*>?,
                        selectedItemView: View,
                        position: Int,
                        id: Long
                    ) {
                        if (closeTime != dropdownClose.selectedItem.toString()){
                            closeTime = dropdownClose.selectedItem.toString()
                            updateSetting()
                        }
                    }

                    override fun onNothingSelected(parentView: AdapterView<*>?) {
                        // your code here
                    }
                }

            tvCoffeeMenu.setOnClickListener { goToMenuList("1") }
            tvNonCoffeeMenu.setOnClickListener { goToMenuList("2") }

            mViewDataBinding.bottom.llOrderList.setOnClickListener {
                goToFragment(parentFragmentManager, OrderListAdminFragment(), Bundle(), "menu list")
            }
        }
    }

    private fun getDataFromFirestore() {
        firebaseDB.collection("setting")
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Toast.makeText(
                        requireContext(),
                        error.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    return@addSnapshotListener
                }

                for (documentMenu: DocumentChange in value?.documentChanges!!) {
                    pref.setSettingCafe(Gson().toJson(documentMenu.document.data).defaultEmpty())
                    setting = documentMenu.document.toObject(Setting::class.java)
                    documentID = documentMenu.document.id
                    statusOpen = setting.statusOpen.defaultEmpty()
                    openTime = setting.openTime.defaultEmpty()
                    closeTime = setting.closingTime.defaultEmpty()
                }

                mViewDataBinding.dropdownStatus.setSelection(
                    statusCafeAdapter.getPosition(setting.statusOpen)
                )

                mViewDataBinding.dropdownOpen.setSelection(
                    openTimeAdapter.getPosition(setting.openTime)
                )

                mViewDataBinding.dropdownClose.setSelection(
                    closeTimeAdapter.getPosition(setting.closingTime)
                )
            }
    }

    private fun updateSetting() {
        if(documentID.isNotEmpty()){
            firebaseDB
                .collection("setting")
                .document(documentID)
                .update(
                    "statusOpen", mViewDataBinding.dropdownStatus.selectedItem.toString().defaultEmpty(),
                    "openTime", mViewDataBinding.dropdownOpen.selectedItem.toString().defaultEmpty(),
                    "closingTime", mViewDataBinding.dropdownClose.selectedItem.toString().defaultEmpty()
                ).addOnSuccessListener {
                    Toast.makeText(requireContext(), "Update Success", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(requireContext(), "Update Failed, Please check internet connection", Toast.LENGTH_SHORT).show()
                }
        }
    }

    private fun goToMenuList(categoryId: String){
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryId
        )
        goToFragment(parentFragmentManager, MenuListAdminFragment(), bundle, "menu")
    }
}