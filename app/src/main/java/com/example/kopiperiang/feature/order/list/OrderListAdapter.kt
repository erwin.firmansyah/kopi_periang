package com.example.kopiperiang.feature.order.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutOrderListBinding
import com.example.kopiperiang.model.schema.Transaction

class OrderListAdapter : RecyclerView.Adapter<OrderListViewHolder>() {

    private lateinit var data: List<Transaction>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListViewHolder {
        val viewBinding: LayoutOrderListBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_order_list, parent, false)
        return OrderListViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized) data.size else 0
    }
    override fun onBindViewHolder(holder: OrderListViewHolder, position: Int) {
        holder.bind(data[position])
    }

    fun updateData(data: List<Transaction>) {
        this.data = data
        notifyDataSetChanged()
    }

}