package com.example.kopiperiang.feature.admin.menulist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuDetailAdminBinding
import com.example.kopiperiang.listener.OnItemClickListenerAdmin

class MenuListAdminAdapter : RecyclerView.Adapter<MenuListAdminViewHolder>() {

    private lateinit var data: List<Menu>
    private lateinit var onItemClickListener: OnItemClickListenerAdmin

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuListAdminViewHolder {
        val viewBinding: LayoutMenuDetailAdminBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_menu_detail_admin, parent, false)
        return MenuListAdminViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized) data.size else 0
    }
    override fun onBindViewHolder(holder: MenuListAdminViewHolder, position: Int) {
        holder.bind(data[position], onItemClickListener)
        holder.itemView.setOnClickListener {
            onItemClickListener.onClick(data[position])
        }
    }

    fun updateData(data: List<Menu>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun setListener(listener: OnItemClickListenerAdmin){
        this.onItemClickListener = listener
    }
}