package com.example.kopiperiang.feature.order.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentOrderListBinding
import com.example.kopiperiang.databinding.FragmentOrderPaymentBinding
import com.example.kopiperiang.extension.setupLinear
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.feature.order.payment.PaymentFragment
import com.example.kopiperiang.listener.OnChangeOrderStatusListener
import com.example.kopiperiang.listener.OnParentActionListener
import com.example.kopiperiang.listener.OnQtyChangeListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.example.kopiperiang.utils.StringHelper
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mu.sekolah.android.extension.defaultEmpty

class OrderListFragment: BaseFragment<EmptyViewModel, FragmentOrderListBinding>(){

    private var fromPayment = false
    private var transactionIDActiveList : ArrayList<String> = arrayListOf()
    private var transactionDataList : ArrayList<Transaction> = arrayListOf()
    private lateinit var orderListAdapter : OrderListAdapter
    private lateinit var onParentActionListener: OnParentActionListener

    override fun setViewModel() {
        if (requireArguments().containsKey(GlobalConstant.Extras.FROM_PAYMENT)) {
            fromPayment = requireArguments().getBoolean(GlobalConstant.Extras.FROM_PAYMENT)
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOrderListBinding =
        FragmentOrderListBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val jsonTransaction = pref.getTransactionListOnState()
        transactionIDActiveList = Gson().fromJson(jsonTransaction, object : TypeToken<List<String?>?>() {}.type) ?: arrayListOf()
        setupView()
    }

    fun setParentListener(listener: OnParentActionListener){
        this.onParentActionListener = listener
    }

    private fun setupView() {
        orderListAdapter = OrderListAdapter()
        mViewDataBinding.rvOrderList.setupLinear(requireContext(), orderListAdapter)
        mViewDataBinding.bottom.llMenuBook.setOnClickListener {
            if(!fromPayment){
                (activity as OrderActivity).transactionLiveCycle = true
                (activity as OrderActivity).onBackPressed()
            }else{
                val bundle = bundleOf(
                    GlobalConstant.Extras.EXTRAS_NEW to true
                )
                goToOrderPage(GlobalConstant.FragmentIndex.MENU, bundle)
            }
        }
        getDataFromFirestore()

        mViewDataBinding.refresh.setOnRefreshListener {
            getDataFromFirestore()
            mViewDataBinding.refresh.isRefreshing = false
        }
    }

    private fun getDataFromFirestore() {
        transactionDataList.clear()
        transactionIDActiveList.forEach {
            it
            firebaseDB.collection("transaction").whereEqualTo("transactionID", it)
                .addSnapshotListener { value, error ->
                    if (error != null) {
                        Toast.makeText(
                            requireContext(),
                            error.message.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                        return@addSnapshotListener
                    }

                    for (documentMenu: DocumentChange in value?.documentChanges!!) {
                        transactionDataList.add(documentMenu.document.toObject(Transaction::class.java))
                    }
                    orderListAdapter.updateData(transactionDataList)
                    mViewDataBinding.tvOrderNull.visibility = if(transactionDataList.size > 0) View.GONE else View.VISIBLE
                }
        }
    }
}