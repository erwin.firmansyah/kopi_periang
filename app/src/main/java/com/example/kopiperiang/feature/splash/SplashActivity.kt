package com.example.kopiperiang.feature.splash

import androidx.fragment.app.Fragment
import com.example.kopiperiang.base.BaseFullScreenActivity
import com.example.kopiperiang.feature.splash.SplashFragment

class SplashActivity : BaseFullScreenActivity() {

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        return newInstanceFragment(bundle, SplashFragment())
    }

}