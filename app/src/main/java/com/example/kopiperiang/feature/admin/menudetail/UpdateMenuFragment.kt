package com.example.kopiperiang.feature.admin.menudetail

import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentHomeAdminBinding
import com.example.kopiperiang.databinding.FragmentOrderListBinding
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.model.schema.Setting
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import mu.sekolah.android.extension.defaultEmpty
import android.widget.AdapterView

import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.widget.addTextChangedListener
import com.example.kopiperiang.databinding.FragmentUpdateMenuBinding
import com.example.kopiperiang.feature.admin.AdminPageActivity
import com.example.kopiperiang.feature.admin.list.OrderListAdminFragment
import com.example.kopiperiang.feature.admin.menulist.MenuListAdminFragment
import com.example.kopiperiang.feature.order.payment.PaymentFragment
import com.example.kopiperiang.listener.OnParentAdminActionListener
import com.example.kopiperiang.listener.OnSeeAllMenuListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import java.util.*


class UpdateMenuFragment: BaseFragment<EmptyViewModel, FragmentUpdateMenuBinding>(){

    private var menu = Menu()
    private var newMenu = true
    private var categoryId = "1"

    override fun setViewModel() {
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_MENU)) {
            menu = requireArguments().getParcelable(GlobalConstant.Extras.EXTRAS_MENU) ?: Menu()
            newMenu = false
        }
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_CATEGORY_ID)) {
            categoryId = requireArguments().getString(GlobalConstant.Extras.EXTRAS_CATEGORY_ID).toString()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentUpdateMenuBinding =
        FragmentUpdateMenuBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    override fun onResume() {
        super.onResume()
        setupView()
    }

    private fun setupView() {
        var statusPage = ""

        mViewDataBinding.apply {

            tvUrlImage.addTextChangedListener {
                GlideHelper.loadImageUrl(requireContext(), tvUrlImage.text.toString(), ivLogo)
                ivDeleteImg.visibility = if(!tvUrlImage.text.toString().isNullOrEmpty()) View.VISIBLE else View.GONE
            }

            ivDeleteImg.setOnClickListener {
                tvUrlImage.setText("")
            }

            if (!newMenu) {
                statusPage = "Edit Menu"
                tvMenuName.setText(menu.menuName.defaultEmpty())
                tvMenuDetail.setText(menu.description.defaultEmpty())
                tvUrlImage.setText(menu.image.defaultEmpty())
                tvPrice.setText(menu.price.defaultEmpty())
                btnSubmit.text = "Selesai Edit"
            } else {
                statusPage = "Tambah Menu"
                val btnSubmitText = "+ Tambahkan"
                btnSubmit.text = btnSubmitText
            }
            val title = when (categoryId) {
                "1" -> "$statusPage - Coffee"
                "2" -> "$statusPage - Non Coffee"
                else -> ""
            }
            tvTitlePage.text = title

            btnSubmit.setOnClickListener {
                if(tvMenuName.text.toString().isEmpty()){
                    Toast.makeText(requireContext(), "Nama Menu Tidak Boleh Kosong", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                if(tvPrice.text.toString().isEmpty()){
                    Toast.makeText(requireContext(), "Harga Tidak Boleh Kosong", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                if(tvMenuDetail.text.toString().isEmpty()){
                    Toast.makeText(requireContext(), "Menu Detail Tidak Boleh Kosong", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }

                val dataMenu = Menu(
                    menuID = UUID.randomUUID().toString(),
                    menuName = tvMenuName.text.toString(),
                    price = tvPrice.text.toString(),
                    description = tvMenuDetail.text.toString(),
                    categoryID = categoryId,
                    image = tvUrlImage.text.toString()
                )
                if (newMenu){
                    firebaseDB.collection("menu")
                        .add(dataMenu)
                        .addOnSuccessListener {
                            (activity as AdminPageActivity).successDialog.startLoading()
                            handler.postDelayed({
                                (activity as AdminPageActivity).successDialog.isDismiss()
                                (activity as AdminPageActivity).customDialog.setupDialog("Berhasil Menambahkan Menu", false, ::nullFun, R.color.green)
                                val bundle = bundleOf(GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryId)
                                goToFragment(parentFragmentManager, MenuListAdminFragment(), bundle, "Menu List")
                            },2000)
                        }
                        .addOnFailureListener {
                            (activity as AdminPageActivity).failedDialog.startLoading()
                            handler.postDelayed({
                                (activity as AdminPageActivity).failedDialog.isDismiss()
                                (activity as AdminPageActivity).customDialog.setupDialog("Gagal Menambah Menu, Silahkan Coba Lagi", false, ::nullFun, R.color.red)
                            },3000)
                        }
                }else{
                    var documentID = ""
                    firebaseDB.collection("menu").whereEqualTo("menuID", menu.menuID)
                        .addSnapshotListener { value, error ->
                            if (error != null) {
                                Toast.makeText(
                                    requireContext(),
                                    error.message.toString(),
                                    Toast.LENGTH_SHORT
                                ).show()
                                return@addSnapshotListener
                            }

                            for (documentMenu: DocumentChange in value?.documentChanges!!) {
                                documentID = documentMenu.document.id
                            }

                            firebaseDB.collection("menu")
                                .document(documentID)
                                .update(
                                    "menuName", tvMenuName.text.toString(),
                                    "price", tvPrice.text.toString(),
                                    "image", tvUrlImage.text.toString(),
                                    "description", tvMenuDetail.text.toString(),
                                ).addOnSuccessListener {
                                    (activity as AdminPageActivity).successDialog.startLoading()
                                    handler.postDelayed({
                                        (activity as AdminPageActivity).successDialog.isDismiss()
                                        (activity as AdminPageActivity).customDialog.setupDialog("Berhasil Mengedit Menu", false, ::nullFun, R.color.green)
                                        goToMenuList()
                                    },2000)
                                }
                                .addOnFailureListener {
                                    (activity as AdminPageActivity).failedDialog.startLoading()
                                    handler.postDelayed({
                                        (activity as AdminPageActivity).failedDialog.isDismiss()
                                        (activity as AdminPageActivity).customDialog.setupDialog("Gagal Mengedit Menu, Silahkan Coba Lagi", false, ::nullFun, R.color.red)
                                        dismissCustomDialog()
                                    },3000)
                                }
                        }
                }
            }
        }
    }

    private fun goToMenuList(){
        val bundle = bundleOf(GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryId)
        handler.postDelayed({
            (activity as AdminPageActivity).customDialog.dismissDialog()
            goToFragment(parentFragmentManager, MenuListAdminFragment(), bundle, "Menu List")
        },2000)
    }

    private fun dismissCustomDialog(){
        handler.postDelayed({
            (activity as AdminPageActivity).customDialog.dismissDialog()
        },2000)
    }

    private fun nullFun(){}
}