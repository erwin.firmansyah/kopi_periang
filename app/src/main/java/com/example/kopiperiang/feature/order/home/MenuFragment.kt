package com.example.kopiperiang.feature.order.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentMenuOrderBinding
import com.example.kopiperiang.extension.setupHorizontal
import com.example.kopiperiang.feature.order.BottomSheetMenuFragment
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.feature.order.detail.MenuDetailFragment
import com.example.kopiperiang.feature.order.list.OrderListFragment
import com.example.kopiperiang.feature.order.payment.PaymentFragment
import com.example.kopiperiang.listener.AddToCartListener
import com.example.kopiperiang.listener.OnItemClickListener
import com.example.kopiperiang.listener.OnParentActionListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import mu.sekolah.android.extension.defaultEmpty
import java.util.*
import kotlin.collections.ArrayList

class MenuFragment : BaseFragment<EmptyViewModel, FragmentMenuOrderBinding>(), OnItemClickListener,OnParentActionListener, AddToCartListener {

    var name : String = ""
    var newTransaction : Boolean = true

    private var dataCoffee : ArrayList<Menu> = arrayListOf()
    private var dataNonCoffee : ArrayList<Menu> = arrayListOf()

    private var bottomSheetMenuFragment : BottomSheetMenuFragment? = null
    private var transaction = Transaction()

    private lateinit var menuHighlightAdapterCoffee : MenuHighlightAdapter
    private lateinit var menuHighlightAdapterNonCoffee : MenuHighlightAdapter


    override fun setViewModel() {
        mViewModel = ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_NEW)) {
            newTransaction = requireArguments().getBoolean(GlobalConstant.Extras.EXTRAS_NEW)
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMenuOrderBinding =
        FragmentMenuOrderBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        name = pref.getCustomerName().defaultEmpty()
        if(!newTransaction){
            transaction = Gson().fromJson(pref.getTransactionData(), Transaction::class.java)
            transaction.customerName = name
        }else{
            setTransaction()
        }
        setupView()
    }

    override fun onClick(menu: Menu) {
        showBottomSheet(menu)
    }

    override fun showBottomSheet(menu: Menu) {
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_MENU to menu
        )
        bottomSheetMenuFragment = BottomSheetMenuFragment.newInstance(bundle)
        bottomSheetMenuFragment?.setListener(this)
        bottomSheetMenuFragment?.show(parentFragmentManager, getString(R.string.empty_string))
    }

    override fun addToCart(menu: Menu) {
        val msg = menu.menuName +" berhasil di tambahkan"
        (activity as OrderActivity).loading.startLoading()
        handler.postDelayed({
            (activity as OrderActivity).loading.isDismiss()
            addToTransaction(menu)
            Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
        }, 1000)
    }

    override fun onFabClick() {
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_TRANSACTION to transaction,
        )
        (activity as OrderActivity).state = 2
        goToFragment(parentFragmentManager, PaymentFragment(), bundle, "Payment")
    }

    override fun onOrderListClick() {
        val bundle = Bundle()
        val orderList = newInstanceOrderList(bundle)
        orderList.setParentListener(this)
        goToFragment(parentFragmentManager, orderList, bundle, "orderList")
        (activity as OrderActivity).state = 3
    }

    override fun onResume() {
        super.onResume()
        (activity as OrderActivity).state = 0
        if((activity as OrderActivity).transactionLiveCycle){
            transaction = Gson().fromJson(pref.getTransactionData(), Transaction::class.java) ?: Transaction()
            (activity as OrderActivity).transactionLiveCycle = false
        }else{
            setTransaction()
        }
    }

    private fun addToTransaction(menu : Menu){
        menu.menuOrderID = UUID.randomUUID().toString()
        transaction.orderList?.add(menu)
    }

    private fun setupView(){
        menuHighlightAdapterCoffee = MenuHighlightAdapter()
        menuHighlightAdapterNonCoffee = MenuHighlightAdapter()
        (activity as OrderActivity).loading.startLoading()
        mViewDataBinding.apply {
            header.tvHelloName.text = getString(R.string.hello_name, name)
            layoutCoffee.tvCategoryName.text = "Coffee"
            layoutCoffee.btnSeeAll.setOnClickListener { goToMenuDetail("1") }
            layoutNonCoffee.btnSeeAll.setOnClickListener { goToMenuDetail("2") }
            layoutNonCoffee.tvCategoryName.text = "Non Coffee"
            layoutCoffee.recyclerView.setupHorizontal(requireContext(), menuHighlightAdapterCoffee)
            layoutNonCoffee.recyclerView.setupHorizontal(requireContext(), menuHighlightAdapterNonCoffee)
            btnPayment.setOnClickListener { onFabClick() }
            bottom.llOrderList.setOnClickListener {
                onOrderListClick()
            }
            mViewDataBinding.groupContent.visibility = View.VISIBLE
            header.btnChangeName.setOnClickListener {
                (activity as OrderActivity).customDialog.setupDialog("Apakah anda yakin ingin mengganti nama?", true,
                    ::changeName, R.color.black)
            }
        }

        firebaseDB.collection("menu")
            .addSnapshotListener { value, error ->
                if(error!=null){
                    Toast.makeText(requireContext(), error.message.toString(), Toast.LENGTH_SHORT).show()
                    return@addSnapshotListener
                }

                for(documentMenu : DocumentChange in value?.documentChanges!!){
                    if(documentMenu.document.toObject(Menu::class.java).categoryID == "1"){
                        dataCoffee.add(documentMenu.document.toObject(Menu::class.java))
                    }else if(documentMenu.document.toObject(Menu::class.java).categoryID == "2"){
                        dataNonCoffee.add(documentMenu.document.toObject(Menu::class.java))
                    }
                }
                menuHighlightAdapterCoffee.updateData(dataCoffee)
                menuHighlightAdapterNonCoffee.updateData(dataNonCoffee)
                handler.postDelayed({
                    (activity as OrderActivity).loading.isDismiss()
                }, 1000)

            }

        menuHighlightAdapterCoffee.setListener(this)
        menuHighlightAdapterNonCoffee.setListener(this)


    }

    private fun goToMenuDetail(categoryID: String){
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryID
        )
        val menuDetail = newInstanceMenuDetail(bundle)
        menuDetail.setParentListener(this)
        goToFragment(parentFragmentManager, menuDetail, bundle, "menuDetail")
        (activity as OrderActivity).state = 1
    }

    private fun newInstanceMenuDetail(bundle: Bundle): MenuDetailFragment {
        val fragment = MenuDetailFragment()
        fragment.arguments = bundle
        return fragment
    }

    private fun newInstanceOrderList(bundle: Bundle): OrderListFragment {
        val fragment = OrderListFragment()
        fragment.arguments = bundle
        return fragment
    }

    private fun setTransaction(){
        transaction = Transaction()
        transaction.transactionID = getRandomString()
        transaction.customerName = name
    }

    private fun getRandomString() : String {
        val allowedChars = ('A'..'Z') + ('0'..'9')
        return (1..6).map { allowedChars.random() }.joinToString("")
    }

    private fun changeName(){
        pref.setTransaction(Gson().toJson(transaction))
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_NEW to false
        )
        goToSetupOrderPage(GlobalConstant.FragmentIndex.CHANGE_NAME, bundle)
    }
}