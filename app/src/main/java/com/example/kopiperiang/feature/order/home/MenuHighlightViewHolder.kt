package com.example.kopiperiang.feature.order.home

import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.databinding.LayoutMenuBinding
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import javax.inject.Inject

class MenuHighlightViewHolder@Inject constructor(private val dataBinding: LayoutMenuBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Menu) {

        dataBinding.apply {
            tvMenuName.text = data.menuName
            val price = "IDR " + StringHelper.convertToCurrencyFormat(data.price.toString(), false)
            tvPrice.text = price
            GlideHelper.loadImageUrl(root.context, data.image.defaultEmpty(), ivMenu)
        }
    }
}