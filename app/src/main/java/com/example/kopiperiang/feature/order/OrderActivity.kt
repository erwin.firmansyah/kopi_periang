package com.example.kopiperiang.feature.order

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFullScreenActivity
import com.example.kopiperiang.feature.order.home.MenuFragment
import com.example.kopiperiang.utils.*

class OrderActivity : BaseFullScreenActivity() {

    var state = 0
    var transactionLiveCycle = false

    val loading = ProgressDialog(this)
    val customDialog = CustomDialog(this)
    var successDialog : SuccessDialogLottie = SuccessDialogLottie(this)
    var failedDialog : FailedDialogLottie = FailedDialogLottie(this)
    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        var fragment = Fragment()

        if (bundle != null && bundle.containsKey(GlobalConstant.Extras.FRAGMENT_INDEX)) {
            fragment = when (bundle.getInt(GlobalConstant.Extras.FRAGMENT_INDEX)) {
                GlobalConstant.FragmentIndex.MENU -> MenuFragment()
                else -> Fragment()

            }
        }
        return newInstanceFragment(bundle, fragment)
    }

    override fun onBackPressed() {
        if(state!=0){
            super.onBackPressed()
        }else{
            customDialog.setupDialog("Apakah anda yakin ingin keluar ?", true,
                { goToSetupOrderPage(GlobalConstant.FragmentIndex.WELCOME_PAGE, Bundle()) }, R.color.black)
        }
    }

    private fun goToSetupOrderPage(fragmentIndex : Int, bundle : Bundle) {
        bundle.putInt(GlobalConstant.Extras.FRAGMENT_INDEX, fragmentIndex)
        val intent = Intent(this, SetupOrderActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }
}