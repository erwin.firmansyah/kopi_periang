package com.example.kopiperiang.feature.order

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseBottomSheetDialogFragment
import com.example.kopiperiang.databinding.FragmentBottomSheetAddMenuBinding
import com.example.kopiperiang.listener.AddToCartListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.GlobalConstant
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import mu.sekolah.android.extension.defaultZero


class BottomSheetMenuFragment : BaseBottomSheetDialogFragment<FragmentBottomSheetAddMenuBinding>(){

    private var menu : Menu = Menu()
    private var qty : Int = 1
    private var totalPrice : Int = 0
    private var iceLevel : String = GlobalConstant.NORMAL
    private var sugarLevel : String = GlobalConstant.NORMAL
    private lateinit var addToCartListener : AddToCartListener

    companion object {
        fun newInstance(bundle: Bundle): BottomSheetMenuFragment {
            val fragment = BottomSheetMenuFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_bottom_sheet_add_menu

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkRequireArguments(GlobalConstant.Extras.EXTRAS_MENU) {
            menu = requireArguments().getParcelable(it) ?: Menu()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        interactionSetup()
    }

    fun setListener(listener : AddToCartListener){
        this.addToCartListener = listener
    }

    private fun interactionSetup(){
        mViewDataBinding.apply {
            layoutEditQty.ivBtnPlus.setOnClickListener {
                qty += 1
                updateView()
            }
            layoutEditQty.ivBtnMinus.setOnClickListener {
                if(qty > 1){
                    qty -= 1
                    updateView()
                }
            }
            ivClose.setOnClickListener {
                dismiss()
            }
            layoutIceLevel.btnNormal.setOnClickListener {
                if(iceLevel!=GlobalConstant.NORMAL){
                    iceLevel = GlobalConstant.NORMAL
                    layoutIceLevel.btnLess.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_white_radius_btn)
                    layoutIceLevel.tvLess.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
                    layoutIceLevel.btnNormal.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_green_radius_btn)
                    layoutIceLevel.tvNormal.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                }
            }
            layoutIceLevel.btnLess.setOnClickListener {
                if(iceLevel!=GlobalConstant.LESS){
                    iceLevel = GlobalConstant.LESS
                    layoutIceLevel.btnLess.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_green_radius_btn)
                    layoutIceLevel.tvLess.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    layoutIceLevel.btnNormal.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_white_radius_btn)
                    layoutIceLevel.tvNormal.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
                }
            }

            layoutSugarLevel.btnNormal.setOnClickListener {
                if(sugarLevel!=GlobalConstant.NORMAL){
                    sugarLevel = GlobalConstant.NORMAL
                    layoutSugarLevel.btnNormal.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_green_radius_btn)
                    layoutSugarLevel.tvNormal.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    layoutSugarLevel.btnLess.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_white_radius_btn)
                    layoutSugarLevel.tvLess.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
                }
            }
            layoutSugarLevel.btnLess.setOnClickListener {
                if(sugarLevel!=GlobalConstant.LESS){
                    sugarLevel = GlobalConstant.LESS
                    layoutSugarLevel.btnNormal.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_white_radius_btn)
                    layoutSugarLevel.tvNormal.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
                    layoutSugarLevel.btnLess.background = ContextCompat.getDrawable(requireContext(), R.drawable.background_green_radius_btn)
                    layoutSugarLevel.tvLess.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                }
            }

            btnAddToCart.setOnClickListener {
                menu.qty = qty.toString()
                menu.sugar = sugarLevel
                menu.ice = iceLevel
                menu.totalPrice = totalPrice
                addToCartListener.addToCart(menu)
                dismiss()
            }
        }
    }

    private fun updateView(){
        mViewDataBinding.apply {
            layoutEditQty.etQty.setText(qty.toString())
            totalPrice = menu.price.defaultZero().toInt() * qty.defaultZero()
            val totalPrice = "IDR " + StringHelper.convertToCurrencyFormat(totalPrice.toString().defaultEmpty(), false).defaultEmpty()
            tvTotalPrice.text = totalPrice
            if(qty>1){
                layoutEditQty.ivBtnMinus.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_btn_minus))
            }else{
                layoutEditQty.ivBtnMinus.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_btn_minus_disable))
            }
        }
    }

    private fun setupView(){
        val price = "IDR " + StringHelper.convertToCurrencyFormat(menu.price.defaultEmpty(), false).defaultEmpty()
        mViewDataBinding.apply {
            qty = layoutEditQty.etQty.text.toString().defaultZero().toInt()
            totalPrice = menu.price.defaultZero().toInt() * qty.defaultZero()
            tvMenuName.text = menu.menuName
            tvPriceMenu.text = price
            tvDescription.text = menu.description
            tvTotalPrice.text = "IDR " + StringHelper.convertToCurrencyFormat(totalPrice.toString().defaultEmpty(), false).defaultEmpty()

            layoutIceLevel.tvTitleLevel.text = "Ice Level"
            layoutSugarLevel.tvTitleLevel.text = "Sugar Level"

            GlideHelper.loadImageUrl(root.context, menu.image.defaultEmpty(), ivMenu)
            if(qty>1){
                layoutEditQty.ivBtnMinus.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_btn_minus))
            }else{
                layoutEditQty.ivBtnMinus.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_btn_minus_disable))
            }
        }
    }
}