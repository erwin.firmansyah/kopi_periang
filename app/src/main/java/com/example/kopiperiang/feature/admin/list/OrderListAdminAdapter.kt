package com.example.kopiperiang.feature.admin.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutOrderListAdminBinding
import com.example.kopiperiang.databinding.LayoutOrderListBinding
import com.example.kopiperiang.listener.OnChangeOrderStatusListener
import com.example.kopiperiang.model.schema.Transaction

class OrderListAdminAdapter : RecyclerView.Adapter<OrderListAdminViewHolder>() {

    private lateinit var data: List<Transaction>
    private lateinit var onChangeOrderStatusListener: OnChangeOrderStatusListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListAdminViewHolder {
        val viewBinding: LayoutOrderListAdminBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_order_list_admin, parent, false)
        return OrderListAdminViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized) data.size else 0
    }
    override fun onBindViewHolder(holder: OrderListAdminViewHolder, position: Int) {
        holder.bind(data[position], onChangeOrderStatusListener)
    }

    fun setListener(listener: OnChangeOrderStatusListener){
        onChangeOrderStatusListener = listener
    }

    fun updateData(data: List<Transaction>) {
        this.data = data
        notifyDataSetChanged()
    }

}