package com.example.kopiperiang.feature.order.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuBinding
import com.example.kopiperiang.listener.OnItemClickListener

class MenuHighlightAdapter : RecyclerView.Adapter<MenuHighlightViewHolder>() {

    private lateinit var data: List<Menu>
    private lateinit var onItemClickListener: OnItemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHighlightViewHolder {
        val viewBinding: LayoutMenuBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_menu, parent, false)
        return MenuHighlightViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized){
            if(data.size>3){
                3
            }else{
                data.size
            }
        } else{
            0
        }
    }
    override fun onBindViewHolder(holder: MenuHighlightViewHolder, position: Int) {
        holder.bind(data[position])
        holder.itemView.setOnClickListener {
            onItemClickListener.onClick(data[position])
        }
    }

    fun updateData(data: List<Menu>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun setListener(listener: OnItemClickListener){
        this.onItemClickListener = listener
    }
}