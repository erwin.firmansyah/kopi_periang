package com.example.kopiperiang.feature.admin.menulist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentMenuBinding
import com.example.kopiperiang.extension.setupLinear
import com.example.kopiperiang.feature.admin.AdminPageActivity
import com.example.kopiperiang.feature.admin.home.HomeAdminFragment
import com.example.kopiperiang.feature.admin.list.OrderListAdminFragment
import com.example.kopiperiang.feature.admin.menudetail.UpdateMenuFragment
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.listener.OnItemClickListener
import com.example.kopiperiang.listener.OnItemClickListenerAdmin
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import kotlin.collections.ArrayList

class MenuListAdminFragment : BaseFragment<EmptyViewModel, FragmentMenuBinding>(),
    OnItemClickListenerAdmin {

    var categoryMenu : String = ""
    var categoryID : String = ""
    private var dataMenu : ArrayList<Menu> = arrayListOf()
    private lateinit var menuListAdapter : MenuListAdminAdapter

    override fun setViewModel() {
        mViewModel = ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_CATEGORY_ID)) {
            categoryID = requireArguments().getString(GlobalConstant.Extras.EXTRAS_CATEGORY_ID).toString()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMenuBinding =
        FragmentMenuBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        menuListAdapter = MenuListAdminAdapter()
        menuListAdapter.setListener(this)
        mViewDataBinding.apply {
            rvMenu.setupLinear(requireContext(), menuListAdapter)
            dataMenu.clear()
            firebaseDB.collection("menu").whereEqualTo("categoryID", categoryID)
                .addSnapshotListener { value, error ->
                    if(error!=null){
                        Toast.makeText(requireContext(), error.message.toString(), Toast.LENGTH_SHORT).show()
                        return@addSnapshotListener
                    }

                    for(documentMenu : DocumentChange in value?.documentChanges!!){
                        dataMenu.add(documentMenu.document.toObject(Menu::class.java))
                    }
                    menuListAdapter.updateData(dataMenu)
                }


            if(!categoryID.isNullOrEmpty()){
                when(categoryID){
                    "1" -> categoryMenu = "Coffee"
                    "2" -> categoryMenu = "Non Coffee"
                }
            }
            val btnText = "+ Tambah Menu" + categoryMenu
            tvBtnAddMenu.text = btnText
            tvBtnAddMenu.setOnClickListener {
                val bundle = bundleOf(
                    GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryID,
                )

                goToFragment(parentFragmentManager, UpdateMenuFragment(), bundle, "update menu")
            }

            bottom.llMenuBook.setOnClickListener {
                goToFragment(parentFragmentManager, HomeAdminFragment(), Bundle(), "home")
            }

            bottom.llOrderList.setOnClickListener {
                goToFragment(parentFragmentManager, OrderListAdminFragment(), Bundle(), "order list")
            }
        }
    }

    override fun onClick(menu: Menu) {
        val bundle = bundleOf(
            GlobalConstant.Extras.EXTRAS_MENU to menu,
            GlobalConstant.Extras.EXTRAS_CATEGORY_ID to categoryID,
        )

        goToFragment(parentFragmentManager, UpdateMenuFragment(), bundle, "update menu")
    }

    override fun onDeleteClick(menu: Menu) {
        (activity as AdminPageActivity).customDialog.setupDialog("Hapus menu \"" + menu.menuName + "\"", true,
            { deleteMenu(menu) }, R.color.black)
    }

    private fun dismissCustomDialog(){
        handler.postDelayed({

        },2000)
    }

    private fun nullFun(){}

    private fun deleteMenu(menu : Menu){
        (activity as AdminPageActivity).customDialog.dismissDialog()
        var documentID = ""
        firebaseDB.collection("menu").whereEqualTo("menuID", menu.menuID)
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Toast.makeText(
                        requireContext(),
                        error.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    return@addSnapshotListener
                }

                for (documentMenu: DocumentChange in value?.documentChanges!!) {
                    documentID = documentMenu.document.id
                }

                firebaseDB.collection("menu")
                    .document(documentID)
                    .delete().addOnSuccessListener {
                        (activity as AdminPageActivity).customDialog.setupDialog("Berhasil Menghapus Menu", false, ::nullFun, R.color.green)
                        handler.postDelayed({
                            (activity as AdminPageActivity).customDialog.dismissDialog()
                            setupView()
                        },2000)
                    }
                    .addOnFailureListener {
                        (activity as AdminPageActivity).customDialog.setupDialog("Gagal Menghapus Menu, Silahkan Coba Lagi", false, ::nullFun, R.color.red)
                        handler.postDelayed({
                            (activity as AdminPageActivity).customDialog.dismissDialog()
                        },2000)
                    }
            }
    }
}