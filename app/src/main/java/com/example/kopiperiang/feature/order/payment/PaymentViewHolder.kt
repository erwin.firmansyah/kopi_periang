package com.example.kopiperiang.feature.order.payment

import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuDetailPaymentBinding
import com.example.kopiperiang.listener.OnQtyChangeListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import mu.sekolah.android.extension.defaultZero
import javax.inject.Inject

class PaymentViewHolder@Inject constructor(private val dataBinding: LayoutMenuDetailPaymentBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Menu, listener: OnQtyChangeListener) {

        dataBinding.apply {
            var qty = data.qty.defaultZero().toInt()

            tvMenuName.text = data.menuName
            val iceLevel = "Ice Level: " + data.ice
            tvIceLevel.text = iceLevel
            val sugarLevel = "Sugar Level: " + data.sugar
            tvSugarLevel.text = sugarLevel
            etQty.setText(qty.toString())
            val formatPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.totalPrice.toString(), false)
            tvPrice.text = formatPrice
            GlideHelper.loadImageUrl(root.context, data.image.defaultEmpty(), ivMenu)
            Log.d("MENU", "MENU NAME : " + data.menuName + " " + data.menuOrderID)
            Log.d("MENU", "MENU QTY : " + qty)
            ivBtnPlus.setOnClickListener {
                qty += 1
                etQty.setText(qty.toString())
                data.totalPrice += data.price.defaultZero().toInt()
                val formatPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.totalPrice.toString(), false)
                tvPrice.text = formatPrice
                listener.onQtyChange(data.menuOrderID.defaultEmpty(), qty)
            }

            ivBtnMinus.setOnClickListener {
                if(qty>1){
                    qty -= 1
                        etQty.setText(qty.toString())
                        data.totalPrice -= data.price.defaultZero().toInt()
                        val formatPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.totalPrice.toString(), false)
                        tvPrice.text = formatPrice
                    listener.onQtyChange(data.menuOrderID.defaultEmpty(), qty)
                }else{
                    listener.onQtyChange(data.menuOrderID.defaultEmpty(), 0)
                }
            }
        }
    }
}