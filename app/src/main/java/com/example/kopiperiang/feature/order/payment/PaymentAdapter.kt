package com.example.kopiperiang.feature.order.payment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuDetailPaymentBinding
import com.example.kopiperiang.listener.OnQtyChangeListener

class PaymentAdapter : RecyclerView.Adapter<PaymentViewHolder>() {

    private lateinit var data: List<Menu>
    private lateinit var onQtyChangeListener: OnQtyChangeListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val viewBinding: LayoutMenuDetailPaymentBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_menu_detail_payment, parent, false)
        return PaymentViewHolder(viewBinding)
    }

    override fun getItemCount(): Int {
        return if (::data.isInitialized) data.size else 0
    }
    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bind(data[position], onQtyChangeListener)
    }

    fun updateData(data: List<Menu>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun setListener(listener: OnQtyChangeListener){
        this.onQtyChangeListener = listener
    }
}