package com.example.kopiperiang.feature.admin

import androidx.fragment.app.Fragment
import com.example.kopiperiang.base.BaseFullScreenActivity
import com.example.kopiperiang.feature.admin.home.HomeAdminFragment
import com.example.kopiperiang.feature.order.home.MenuFragment
import com.example.kopiperiang.feature.order.insertname.InsertNameFragment
import com.example.kopiperiang.feature.order.landingpage.OpenOrderFragment
import com.example.kopiperiang.utils.*

class AdminPageActivity : BaseFullScreenActivity() {

    val customDialog = CustomDialog(this)
    var successDialog : SuccessDialogLottie = SuccessDialogLottie(this)
    var failedDialog : FailedDialogLottie = FailedDialogLottie(this)

    override fun getFragmentContainer(): Fragment {
        val bundle = intent.extras
        var fragment = Fragment()

        if (bundle != null && bundle.containsKey(GlobalConstant.Extras.FRAGMENT_INDEX)) {
            fragment = when (bundle.getInt(GlobalConstant.Extras.FRAGMENT_INDEX)) {
                GlobalConstant.FragmentIndex.ADMIN_PAGE -> HomeAdminFragment()
                else -> Fragment()

            }
        }
        return newInstanceFragment(bundle, fragment)
    }
}