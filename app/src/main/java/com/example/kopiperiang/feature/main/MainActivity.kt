package com.example.kopiperiang.feature.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFullStateActivity
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.ActivityMainBinding

class MainActivity : BaseFullStateActivity<EmptyViewModel, ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mViewDataBinding.toolbar)
    }

    override fun setViewModel() {
        mViewModel = ViewModelProvider(this).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(inflater: LayoutInflater): ActivityMainBinding {
        return ActivityMainBinding.inflate(inflater)
    }

    override fun getFragmentContainer(): Fragment? = null

    override fun getMainContainer(): View? = null

}