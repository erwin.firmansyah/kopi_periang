package com.example.kopiperiang.feature.admin.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import com.example.kopiperiang.R
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentOrderListAdminBinding
import com.example.kopiperiang.databinding.FragmentOrderListBinding
import com.example.kopiperiang.extension.setupLinear
import com.example.kopiperiang.feature.admin.AdminPageActivity
import com.example.kopiperiang.feature.admin.home.HomeAdminFragment
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.listener.OnChangeOrderStatusListener
import com.example.kopiperiang.listener.OnParentActionListener
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlobalConstant
import com.example.kopiperiang.utils.StringHelper
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class OrderListAdminFragment: BaseFragment<EmptyViewModel, FragmentOrderListAdminBinding>(), OnChangeOrderStatusListener{

    private var fromPayment = false
    private var transactionIDActiveList : ArrayList<String> = arrayListOf()
    private var transactionDataList : ArrayList<Transaction> = arrayListOf()
    private lateinit var orderListAdapter : OrderListAdminAdapter

    override fun setViewModel() {
        if (requireArguments().containsKey(GlobalConstant.Extras.FROM_PAYMENT)) {
            fromPayment = requireArguments().getBoolean(GlobalConstant.Extras.FROM_PAYMENT)
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentOrderListAdminBinding =
        FragmentOrderListAdminBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val jsonTransaction = pref.getTransactionListOnState()
        transactionIDActiveList = Gson().fromJson(jsonTransaction, object : TypeToken<List<String?>?>() {}.type) ?: arrayListOf()
        setupView()
    }

    private fun setupView() {
        orderListAdapter = OrderListAdminAdapter()
        orderListAdapter.setListener(this)
        mViewDataBinding.rvOrderList.setupLinear(requireContext(), orderListAdapter)
        getDataFromFirestore()
        mViewDataBinding.bottom.llMenuBook.setOnClickListener {
            goToFragment(parentFragmentManager, HomeAdminFragment(), Bundle(), "home")
        }

        mViewDataBinding.refresh.setOnRefreshListener {
            getDataFromFirestore()
            mViewDataBinding.refresh.isRefreshing = false
        }
    }

    private fun getDataFromFirestore() {
        transactionDataList.clear()
        firebaseDB.collection("transaction").whereEqualTo("orderDate", StringHelper.getDate())
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Toast.makeText(
                        requireContext(),
                        error.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    return@addSnapshotListener
                }

                for (documentMenu: DocumentChange in value?.documentChanges!!) {
                    transactionDataList.add(documentMenu.document.toObject(Transaction::class.java))
                }
                orderListAdapter.updateData(transactionDataList)
                mViewDataBinding.tvOrderNull.visibility = if(transactionDataList.size > 0) View.GONE else View.VISIBLE
            }
    }

    override fun onChange(transaction: Transaction) {
        var documentID = ""
        firebaseDB.collection("transaction").whereEqualTo("transactionID", transaction.transactionID)
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Toast.makeText(
                        requireContext(),
                        error.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    return@addSnapshotListener
                }

                for (documentMenu: DocumentChange in value?.documentChanges!!) {
                    documentID = documentMenu.document.id
                }

                firebaseDB.collection("transaction")
                    .document(documentID)
                    .update(
                        "orderStatus", transaction.orderStatus
                    ).addOnSuccessListener {
                        Toast.makeText(requireContext(), "Berhasil Mengubah Status (${transaction.transactionID})", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Gagal Mengubah Status (${transaction.transactionID})", Toast.LENGTH_SHORT).show()
                    }
            }
    }
}