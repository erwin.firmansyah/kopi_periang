package com.example.kopiperiang.feature.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentSplashScreenBinding
import com.example.kopiperiang.model.schema.Setting
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import com.google.gson.Gson
import java.util.*
import kotlin.concurrent.schedule

class SplashFragment : BaseFragment<EmptyViewModel, FragmentSplashScreenBinding>() {

    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentSplashScreenBinding =
        FragmentSplashScreenBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getDataSettingFirestore()
        Timer().schedule(3000) {
            goToSetupOrderPage(GlobalConstant.FragmentIndex.WELCOME_PAGE, Bundle())
            requireActivity().finish()
        }
    }

    private fun getDataSettingFirestore() {
        firebaseDB.collection("setting")
            .addSnapshotListener { value, error ->
                if (error != null) {
                    Toast.makeText(
                        requireContext(),
                        error.message.toString(),
                        Toast.LENGTH_SHORT
                    ).show()
                    return@addSnapshotListener
                }

                for (documentMenu: DocumentChange in value?.documentChanges!!) {
                    pref.setSettingCafe(Gson().toJson(documentMenu.document.toObject(Setting::class.java)))
                }
            }
    }
}