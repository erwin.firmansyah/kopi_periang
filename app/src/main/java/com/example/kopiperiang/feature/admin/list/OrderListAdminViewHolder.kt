package com.example.kopiperiang.feature.admin.list

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuOrderListBinding
import com.example.kopiperiang.databinding.LayoutOrderListAdminBinding
import com.example.kopiperiang.listener.OnChangeOrderStatusListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultZero
import mu.sekolah.android.extension.withDefault
import javax.inject.Inject

class OrderListAdminViewHolder@Inject constructor(private val dataBinding: LayoutOrderListAdminBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Transaction, onChangeOrderStatusListener: OnChangeOrderStatusListener) {

        dataBinding.apply {
            var menuVisibility = false
            val transactionID = "("+data.transactionID+")"
            val orderID = data.customerName + " " + transactionID
            tvCustomerName.text = orderID
            val totalItem = data.orderList?.size.toString() + " Item"
            tvTotalItem.text = totalItem
            val totalPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.grandTotal.toString(), false)
            tvPrice.text = totalPrice
            tvTableName.text = data.tableNo
            tvDate.text = data.transactionDate

            var statusOrder = ""

            ArrayAdapter.createFromResource(
                root.context,
                R.array.status,
                R.layout.spinner_text_order_status
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                dropdownStatus.adapter = adapter
            }

            dropdownStatus.setSelection(data.orderStatus.withDefault(1)-1)

            when(data.orderStatus){
                1 -> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_pending)
                    statusOrder = "Pending"
                }
                2 -> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_success)
                    statusOrder = "Prepared"
                }
                3-> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_success)
                    statusOrder = "Completed"
                }
            }

            val orderList = data.orderList ?: arrayListOf()
            addMenuList(root.context, llDetailMenu, orderList)
            llDetailMenu.visibility = if(menuVisibility) View.VISIBLE else View.GONE

            cvDetailMenu.setOnClickListener {
                menuVisibility = !menuVisibility
                llDetailMenu.visibility = if(menuVisibility) View.VISIBLE else View.GONE
            }

            dropdownStatus.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parentView: AdapterView<*>?,
                        selectedItemView: View,
                        position: Int,
                        id: Long
                    ) {
                        if (statusOrder != dropdownStatus.selectedItem.toString()){
                            statusOrder = dropdownStatus.selectedItem.toString()
                            data.orderStatus = position+1
                            onChangeOrderStatusListener.onChange(data)
                        }
                    }

                    override fun onNothingSelected(parentView: AdapterView<*>?) {
                        // your code here
                    }
                }

        }
    }

    private fun addMenuList(context: Context, llMenuOrderList: LinearLayoutCompat, listMenu: List<Menu>) {

        llMenuOrderList.removeAllViews()
        listMenu.forEach { menu ->
            val view = View.inflate(context, R.layout.layout_menu_order_list, null)
            val binding = LayoutMenuOrderListBinding.bind(view)
            binding.apply {
                tvMenuName.text = menu.menuName
                val ice = "Ice Level : " + menu.ice
                tvIceLevel.text = ice
                val sugar = "Sugar Level : " + menu.ice
                tvSugarLevel.text = sugar
                tvQty.text = menu.qty
            }
            llMenuOrderList.addView(binding.root)
        }
    }
}