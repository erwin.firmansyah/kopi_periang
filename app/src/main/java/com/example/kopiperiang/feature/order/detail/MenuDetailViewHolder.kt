package com.example.kopiperiang.feature.order.detail

import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.databinding.LayoutMenuDetailBinding
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import javax.inject.Inject

class MenuDetailViewHolder@Inject constructor(private val dataBinding: LayoutMenuDetailBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Menu) {

        dataBinding.apply {
            tvMenuName.text = data.menuName
            val itemPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.price.defaultEmpty(), false).defaultEmpty()
            tvPrice.text = itemPrice
            tvDescription.text = data.description
            GlideHelper.loadImageUrl(root.context, data.image.defaultEmpty(), ivMenu)
        }
    }
}