package com.example.kopiperiang.feature.order.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.kopiperiang.base.BaseFragment
import com.example.kopiperiang.base.EmptyViewModel
import com.example.kopiperiang.databinding.FragmentMenuOrderDetailBinding
import com.example.kopiperiang.extension.setupLinear
import com.example.kopiperiang.feature.order.OrderActivity
import com.example.kopiperiang.listener.OnItemClickListener
import com.example.kopiperiang.listener.OnParentActionListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.utils.GlobalConstant
import com.google.firebase.firestore.DocumentChange
import kotlin.collections.ArrayList

class MenuDetailFragment : BaseFragment<EmptyViewModel, FragmentMenuOrderDetailBinding>(), OnItemClickListener {

    var categoryMenu : String = ""
    var categoryID : String = ""
    private var dataMenu : ArrayList<Menu> = arrayListOf()
    private lateinit var menuDetailAdapter : MenuDetailAdapter
    private lateinit var onParentActionListener: OnParentActionListener

    override fun setViewModel() {
        mViewModel = ViewModelProvider(requireActivity()).get(EmptyViewModel::class.java)
        if (requireArguments().containsKey(GlobalConstant.Extras.EXTRAS_CATEGORY_ID)) {
            categoryID = requireArguments().getString(GlobalConstant.Extras.EXTRAS_CATEGORY_ID).toString()
        }
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentMenuOrderDetailBinding =
        FragmentMenuOrderDetailBinding.inflate(layoutInflater, container, false)

    override fun getMainContainer(): View? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    fun setParentListener(listener: OnParentActionListener){
        this.onParentActionListener = listener
    }

    private fun setupView(){
        dataMenu.clear()
        (activity as OrderActivity).transactionLiveCycle = true
        menuDetailAdapter = MenuDetailAdapter()
        mViewDataBinding.rvMenu.setupLinear(requireContext(), menuDetailAdapter)
        firebaseDB.collection("menu").whereEqualTo("categoryID", categoryID)
            .addSnapshotListener { value, error ->
                if(error!=null){
                    Toast.makeText(requireContext(), error.message.toString(), Toast.LENGTH_SHORT).show()
                    return@addSnapshotListener
                }

                for(documentMenu : DocumentChange in value?.documentChanges!!){
                    dataMenu.add(documentMenu.document.toObject(Menu::class.java))
                }
                menuDetailAdapter.updateData(dataMenu)
            }

        menuDetailAdapter.setListener(this)

        if(!categoryID.isNullOrEmpty()){
            when(categoryID){
                "1" -> categoryMenu = "Coffee"
                "2" -> categoryMenu = "Non Coffee"
            }
        }
        mViewDataBinding.tvCategoryName.text = categoryMenu
        mViewDataBinding.btnPayment.setOnClickListener { onParentActionListener.onFabClick() }
        mViewDataBinding.bottom.llMenuBook.setOnClickListener {
            (activity as OrderActivity).onBackPressed()
        }
        mViewDataBinding.bottom.llOrderList.setOnClickListener {
            onParentActionListener.onOrderListClick()
        }
    }

    override fun onClick(menu: Menu) {
        onParentActionListener.showBottomSheet(menu)
    }
}