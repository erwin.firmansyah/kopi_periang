package com.example.kopiperiang.feature.order.list

import android.content.Context
import android.util.Log
import android.view.View
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.kopiperiang.R
import com.example.kopiperiang.databinding.LayoutMenuDetailPaymentBinding
import com.example.kopiperiang.databinding.LayoutMenuOrderListBinding
import com.example.kopiperiang.databinding.LayoutOrderListBinding
import com.example.kopiperiang.listener.OnQtyChangeListener
import com.example.kopiperiang.model.schema.Menu
import com.example.kopiperiang.model.schema.Transaction
import com.example.kopiperiang.utils.GlideHelper
import com.example.kopiperiang.utils.StringHelper
import mu.sekolah.android.extension.defaultEmpty
import mu.sekolah.android.extension.defaultZero
import javax.inject.Inject

class OrderListViewHolder@Inject constructor(private val dataBinding: LayoutOrderListBinding) :
    RecyclerView.ViewHolder(dataBinding.root) {

    fun bind(data: Transaction) {

        dataBinding.apply {
            var menuVisibility = false
            val transactionID = "("+data.transactionID+")"
            val orderID = data.customerName + " " + transactionID
            tvCustomerName.text = orderID
            val totalItem = data.orderList?.size.toString() + " Item"
            tvTotalItem.text = totalItem
            val totalPrice = "IDR " + StringHelper.convertToCurrencyFormat(data.grandTotal.toString(), false)
            tvPrice.text = totalPrice
            tvTableName.text = data.tableNo
            tvDate.text = data.transactionDate

            when(data.orderStatus){
                1 -> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_pending)
                    tvStatus.text = "Pending"
                    tvStatus.setTextColor(ContextCompat.getColor(root.context, R.color.pending))
                }
                2 -> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_success)
                    tvStatus.text = "Prepared"
                    tvStatus.setTextColor(ContextCompat.getColor(root.context, R.color.prepared))
                }
                3-> {
                    ivPaymentStatus.setImageResource(R.drawable.payment_success)
                    tvStatus.text = "Completed"
                    tvStatus.setTextColor(ContextCompat.getColor(root.context, R.color.green))
                }
            }
            val orderList = data.orderList ?: arrayListOf()
            addMenuList(root.context, llDetailMenu, orderList)
            llDetailMenu.visibility = if(menuVisibility) View.VISIBLE else View.GONE

            cvDetailMenu.setOnClickListener {
                menuVisibility = !menuVisibility
                llDetailMenu.visibility = if(menuVisibility) View.VISIBLE else View.GONE
            }
        }
    }

    private fun addMenuList(context: Context, llMenuOrderList: LinearLayoutCompat, listMenu: List<Menu>) {

        llMenuOrderList.removeAllViews()
        listMenu.forEach { menu ->
            val view = View.inflate(context, R.layout.layout_menu_order_list, null)
            val binding = LayoutMenuOrderListBinding.bind(view)
            binding.apply {
                tvMenuName.text = menu.menuName
                val ice = "Ice Level : " + menu.ice
                tvIceLevel.text = ice
                val sugar = "Sugar Level : " + menu.ice
                tvSugarLevel.text = sugar
                tvQty.text = menu.qty
            }
            llMenuOrderList.addView(binding.root)
        }
    }
}