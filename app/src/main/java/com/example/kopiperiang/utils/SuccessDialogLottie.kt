package com.example.kopiperiang.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import com.example.kopiperiang.R

class SuccessDialogLottie(private val activity : Activity) {
    private lateinit var isDialog : Dialog
    fun startLoading(){
        isDialog = Dialog(activity)
        isDialog.setContentView(R.layout.success_dialog)
        isDialog.setCancelable(true)
        isDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        isDialog.show()
    }

    fun isDismiss(){
        isDialog.dismiss()
    }
}