package com.example.kopiperiang.utils

class GlobalConstant {

    companion object {
        const val API_KEY = "Z1hGgh0SM5PH4EureViMJ0lCgDSA1ubb"
        const val EMPTY_STRING = ""
        const val TOOLBAR_TITLE = "toolbar_title"
        const val TOOLBAR_BACKGROUND_COLOR = "toolbar_background_color"
        const val NORMAL = "Normal"
        const val LESS = "Less"
        const val LOCALE_ID = "id"
    }

    object Extras {
        const val FRAGMENT_INDEX = "fragment_index"
        const val FROM_PAYMENT = "from_payment"
        const val EXTRAS_NEW = "new"
        const val EXTRAS_NAME = "name"
        const val EXTRAS_ADMIN_PASSWORD = "admin_password"
        const val EXTRAS_INITIATE_FRAGMENT = "extras_init_transaction"
        const val EXTRAS_TRANSACTION = "extras_transaction"
        const val EXTRAS_SETTING = "extras_setting"
        const val EXTRAS_TRANSACTION_LIST= "extras_transaction_list"
        const val EXTRAS_MENU = "menu"
        const val EXTRAS_CATEGORY_ID = "category_id"
    }

    object FragmentIndex {
        const val WELCOME_PAGE = 1
        const val ADMIN_PAGE = 2
        const val MENU = 3
        const val CHANGE_NAME = 4
    }

    enum class NavigationType {
        HOME, ORDER
    }
}