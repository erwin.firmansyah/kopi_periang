package com.example.kopiperiang.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.kopiperiang.R

object GlideHelper {

    fun loadImageUrl(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
            .load(url)
            .apply(RequestOptions.errorOf(R.drawable.img_not_found).placeholder(R.drawable.img_not_found))
            .into(imageView)
    }

}