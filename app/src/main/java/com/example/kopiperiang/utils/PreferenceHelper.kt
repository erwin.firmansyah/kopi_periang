package com.example.kopiperiang.utils

import android.content.Context
import android.content.SharedPreferences
import mu.sekolah.android.extension.defaultEmpty

class PreferenceHelper(context : Context) {

    private val pref = "KopiPeriang"
    private var sharedPref : SharedPreferences = context.getSharedPreferences(pref, Context.MODE_PRIVATE)
    private var editor : SharedPreferences.Editor = sharedPref.edit()

    init{
        editor.apply()
    }

    fun setTransaction(data : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_TRANSACTION, data)
        editor.commit()
    }

    fun getTransactionData(): String{
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_TRANSACTION, "").defaultEmpty()
    }

    fun setTransactionListOnState(data : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_TRANSACTION_LIST, data)
        editor.commit()
    }

    fun getTransactionListOnState(): String? {
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_TRANSACTION_LIST, "").defaultEmpty()
    }

    fun setCustomerName(name : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_NAME, name)
        editor.commit()
    }

    fun getCustomerName(): String? {
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_NAME, "").defaultEmpty()
    }

    fun setAdminPassword(name : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_ADMIN_PASSWORD, name)
        editor.commit()
    }

    fun getAdminPassword(): String? {
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_ADMIN_PASSWORD, "123123").defaultEmpty()
    }

    fun setSettingCafe(data : String){
        editor.putString(GlobalConstant.Extras.EXTRAS_SETTING, data)
        editor.commit()
    }

    fun getSettingCafe(): String{
        return sharedPref.getString(GlobalConstant.Extras.EXTRAS_SETTING, "").defaultEmpty()
    }

}