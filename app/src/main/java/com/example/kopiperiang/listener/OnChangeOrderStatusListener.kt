package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Transaction

interface OnChangeOrderStatusListener {
    fun onChange(transaction: Transaction)
}