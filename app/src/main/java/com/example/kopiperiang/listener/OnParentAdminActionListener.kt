package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Menu

interface OnParentAdminActionListener {
    fun onMenuClick(menu : Menu)
    fun onCategoryMenuClick(categoryId : String)
}