package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Menu

interface AddToCartListener {
    fun addToCart(menu : Menu)
}