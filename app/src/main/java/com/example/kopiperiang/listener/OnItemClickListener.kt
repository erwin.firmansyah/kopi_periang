package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Menu

interface OnItemClickListener {
    fun onClick(menu : Menu)
}