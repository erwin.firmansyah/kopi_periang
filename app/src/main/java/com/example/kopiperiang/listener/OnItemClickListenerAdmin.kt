package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Menu

interface OnItemClickListenerAdmin {
    fun onClick(menu : Menu)
    fun onDeleteClick(menu : Menu)
}