package com.example.kopiperiang.listener

interface OnQtyChangeListener {
    fun onQtyChange(menuOrderID : String, qty : Int)
}