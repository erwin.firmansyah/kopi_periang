package com.example.kopiperiang.listener

import com.example.kopiperiang.model.schema.Menu

interface OnParentActionListener {
    fun showBottomSheet(menu : Menu)
    fun onFabClick()
    fun onOrderListClick()
}