package mu.sekolah.android.extension

import android.content.Context
import java.io.IOException
import java.io.InputStream
import java.security.MessageDigest

fun String.formatToPhoneNumber(): String {
    var phone = this
    if (contains("+62")) {
        phone = "0${substring(3, length)}"
    }
    if (equals("+62", true)) {
        phone = ""
    }
    return phone
}

fun String?.formatPhoneNumberWithCountryCode(): String {
    var phone = this
    if (!isNullOrEmpty() && !contains("+62")) {
        phone = "+62${substring(1, length)}"
    }
    return phone.defaultEmpty()
}

fun String?.formatPhoneNumberWithZero(): String {
    var phone = this
    if (!isNullOrEmpty() && contains("+62")) {
        phone = "0${substring(3, length)}"
    }
    return phone.defaultEmpty()
}

fun loadJSONFromAsset(context: Context, filename: String): String {
    return try {
        val inputStream: InputStream = context.assets.open("json/$filename")
        val size: Int = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        String(buffer, charset("UTF-8"))
    } catch (ex: IOException) {
        ex.printStackTrace()
        ""
    }
}

/**
 * Use dash / "-" just only for ui needed.
 * Not recommended for conditional data. ex if (value == ....)
 */
fun String?.defaultDash(): String = if (isNullOrEmpty()) "-" else this

fun String?.defaultEmpty(): String = if (isNullOrEmpty()) "" else this

fun String?.defaultZero(): String = if (isNullOrEmpty()) "0" else this

fun String?.defaultDoubleZero(): String = if (isNullOrEmpty()) "0.0" else this

fun String?.withDefault(default: String): String = if (isNullOrEmpty()) default else this

fun String?.asFloat(): Float {
    val char = if (isNullOrEmpty() || isNumber()) "0" else this
    return char.toFloat()
}

fun String?.asInt(): Int {
    val char = if (isNullOrEmpty() || !isNumber()) "0" else this
    return char.toInt()
}

fun String?.isNumber(): Boolean {
    return try {
        this?.toInt()
        true
    } catch (e: Exception) {
        false
    }
}

// https://www.javacodemonk.com/md5-and-sha256-in-java-kotlin-and-android-96ed9628
fun String.toMD5(): String {
    val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return bytes.toHex()
}

fun ByteArray.toHex(): String {
    return joinToString("") { "%02x".format(it) }
}

fun Double.format2Digit(number: Double): String {
    return String.format("%.2f", number)
}